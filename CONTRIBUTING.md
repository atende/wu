Contributing to WebUtil
=================================================================

# Building

The build is made with gradle. You don't need to install, you can use the wrapper, just run the *gradlew* script in the root folder

First make sure all subprojects are cloned with the command:

      git submodule init
      git submodule update
      #checkout all projects to master
      for i in wu-*; do cd $i; git checkout master; cd ..; done 

Be aware that git submodule clone the modules to a specific version, you need to checkout each submodule to branch *master*.
This is done in the last command

Now Run the build

    ./gradlew build
    
This will build all projects an run all tests.

# Submitting a Pull Request

Before you submit your pull request consider the following guidelines:

* Search Bitbucket for an open or closed Pull Request that relates to your submission. You don't want to duplicate effort.
* Make your changes in a new git branch. 
* We follow [Git Flow](http://nvie.com/posts/a-successful-git-branching-model/) feature branch's.
*     If you create a new feature checkout from **develop**, if you create a fix checkout from **master**

        git checkout -b fix/my-fix-branch master

      Or 
    
        git checkout -b feature/my-feature-branch develop

* Create your patch, including appropriate test cases.

* Follow our [Coding Rules](#rule).
* Run the full JUnit test suite, as described in the developer documentation, and ensure that all tests pass.
*     Commit your changes using a descriptive commit message that follows our commit message conventions.

        git commit -a

Note: the optional commit ``-a`` command line option will automatically "add" and "rm" edited files.

*     Push your branch to Bitbucket:

        git push origin my-fix-branch

* In Bitbucket, send a pull request to **develop** branch.

* If we suggest changes then
    - Make the required updates.
    - Re-run the JUnit test suite to ensure tests are still passing.
    - Rebase your branch and force push to your Bitbucket repository (this will update your Pull Request):

        git rebase master -i
        git push -f
    
That's it! Thank you for your contribution!

## After your pull request is merged

After your pull request is merged, you can safely delete your branch and pull the changes from the main (upstream) repository:

Delete the remote branch on Bitbucket either through the Bitbucket web UI or your local shell as follows:

    git push origin --delete my-fix-branch
    
Check out the master branch:

    git checkout master -f
    
Delete the local branch:

    git branch -D my-fix-branch
    
Update your master with the latest upstream version:

    git pull --ff upstream master


# <a name="rule"></a> Coding Rules

To ensure consistency throughout the source code, keep these rules in mind as you are working:

* All features or bug fixes must be tested by one or more specs.
* All public API methods must be documented with doc comments.
* We follow the Java Style created by Intellij IDEA defaults.

## Commit Message Format

Each commit message consists of a header, a body and a footer. The header has a special format that includes a type, a scope and a subject:

    <type>(<scope>): <subject>
    <BLANK LINE>
    <body>
    <BLANK LINE>
    <footer>

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier to read on bitbucket as well as in various git tools.

### Type

Must be one of the following:

* feat: A new feature
* fix: A bug fix
* docs: Documentation only changes
* style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* refactor: A code change that neither fixes a bug or adds a feature
* perf: A code change that improves performance
* test: Adding missing tests
* chore: Changes to the build process or auxiliary tools and libraries such as documentation generation

### Scope

The scope could be anything specifying place of the commit change. For example *Config*