WebUtil
=================================================================

A collection of libraries for Web Development using Java Enterprise Edition specifications.

## Installing

Just use maven or gradle for one of the subprojects

**groupId:** info.atende.webutil

**artifactId:** cdi, jpa, rest

 
## Documentation

[http://wiki.atende.info/display/wu](http://wiki.atende.info/display/wu)

## Filling Bugs

Please file bugs in [Jira](http://projetos.atende.info/browse/wu)

## I want help

Very good. Read [contributing](contributing)